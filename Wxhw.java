import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import com.google.gson.*;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.*;
import java.text.*;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Wxhw
{
   private final String key = "f6102a4c470fc1247655533d2c946a40";
   private String zipcode;
   private JsonElement json;
   public String date;
      
   public Wxhw(){
   }
      
   public Wxhw(String z)
   {
      zipcode = z;
   }
   
   public void fetch()
   {
      String weatherRequest = "https://api.openweathermap.org/data/2.5/weather?apikey="
                               + key + "&zip=" + zipcode + ",us&units=imperial";
      try
      {
         URL wxhwURL = new URL(weatherRequest);
         
         InputStream is = wxhwURL.openStream();
         InputStreamReader isr = new InputStreamReader(is);
         BufferedReader br = new BufferedReader(isr);
         
         JsonParser parser = new JsonParser();
         json = parser.parse(br);
      }
      catch (java.io.UnsupportedEncodingException uee)
      {
         uee.printStackTrace();
      }
      catch (java.net.MalformedURLException mue)
      {
         mue.printStackTrace();
      }
      catch (java.io.IOException ioe)
      {
         ioe.printStackTrace();
      }
   }
   
   public String getLocation()
   {
      return json.getAsJsonObject().get("name").getAsString();
   }
   
   public String getTime()
   {
      long unix_seconds = Long.parseLong(json.getAsJsonObject().get("dt").getAsString());
      Date date = new Date(unix_seconds * 1000L);
      SimpleDateFormat jdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
      jdf.setTimeZone(TimeZone.getTimeZone("PST"));
      String java_date = jdf.format(date);
      return java_date;
   }
   
   public String getWeather()
   {
      return json.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("main").getAsString();
   }
   
   public String getTemperature()
   {
      return json.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
   }
   
   public String getWindDirection()
   {
      return json.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString();
   }
   
   public String getWindSpeed()
   {
      return json.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsString();
   }
   
   public String getPressure()
   {
      double hPa = json.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsDouble();
      String k = toINHG(hPa);
      return k;
   }
   
   public static void request(String zip)
   {
      Wxhw u = new Wxhw(zip);
      if(!isValid(zip)) {
         System.out.print("ERROR: No cities match your search query");
      } else {
         u.fetch();
      
         System.out.println("Location:      " + u.getLocation());
         System.out.println("Time:          " + u.getTime());
         System.out.println("Weather:       " + u.getWeather());
         System.out.println("Temperature F: " + u.getTemperature());
         System.out.println("Wind:          From " + u.getWindDirection() + " degrees at " + u.getWindSpeed() + "MPH");
         System.out.print("Pressure inHG: " + u.getPressure());
      }
   }
   
   public static boolean isValid(String z)
   {
      //test if zip is a six digit number
      if(z != "000000")
      {
         List<String> zips = new ArrayList<String>();
       
         zips.add(z);  
      
         String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
      
         Pattern pattern = Pattern.compile(regex);
      
         for (String zip : zips)
         {
            Matcher matcher = pattern.matcher(zip);
            return (matcher.matches());
         }
      }
      return false;
   }
   
   public static String toINHG(double hPa)
   {
      double inHg = hPa * 0.03;
      inHg = Math.round((inHg * 10000.0) / 10000.0);
      String s = Double.toString(inHg);
      return s;
   }
   
   public static void main(String[] args) {
      String zip;
      
      if (args.length == 0)
      {
         System.out.print("You need to enter a zip code");
         System.exit(1);
      }
      
      zip = args[0];
      
      try {
         request(zip);
      } catch (Exception e) {
         System.out.print("ERROR: No cities match your search query");
      }
   
   }
      
}